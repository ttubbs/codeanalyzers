# Maven

## Usage in Java projects

Add the following profile to your project's pom.  This will configure the CheckStyle, PMD and FindBugs Maven plug-ins with dependencies to this project
ensuring you're always running with the latest changes.  Making use of a profile makes it easy to toggle the checks on/off so they are only executed when desired.

```xml
    <profiles>

        <!-- Code Analysis-->
        <profile>
            <id>code-analysis</id>
            <activation>
                <property>
                    <!-- Activate by setting property. e.g. vm arg -Danalyze=true -->
                    <name>analyze</name>
                </property>
            </activation>

            <!-- One-Stop config block for analysis tools. Should only need to make adjustments here. -->
            <properties>
                <codeanalyzers.version>trunk-SNAPSHOT</codeanalyzers.version>
                <analysis.checkstyle.phase>process-resources</analysis.checkstyle.phase>
                <analysis.checkstyle.logToConsole>true</analysis.checkstyle.logToConsole>
                <analysis.checkstyle.failOnViolation>true</analysis.checkstyle.failOnViolation>
                <analysis.checkstyle.maxAllowedViolations>10</analysis.checkstyle.maxAllowedViolations>

                <analysis.pmd.phase>process-resources</analysis.pmd.phase>
                <analysis.pmd.logToConsole>true</analysis.pmd.logToConsole>
                <analysis.pmd.includeTests>true</analysis.pmd.includeTests>
                <analysis.pmd.failOnViolation>true</analysis.pmd.failOnViolation>
                <analysis.cpd.failOnViolation>true</analysis.cpd.failOnViolation>

                <analysis.findbugs.phase>compile</analysis.findbugs.phase>
                <analysis.findbugs.failOnViolation>false</analysis.findbugs.failOnViolation>
                <analysis.findbugs.includeTests>true</analysis.findbugs.includeTests>
            </properties>

            <!-- Analysis Tool Executions -->
            <build>
                <plugins>
                    <!-- CheckStyle -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-checkstyle-plugin</artifactId>
                        <version>3.0.0</version>
                        <executions>
                            <execution>
                                <id>checkstyle-analysis</id>
                                <phase>${analysis.checkstyle.phase}</phase>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                        <dependencies>
                            <dependency>
                                <groupId>com.puppycrawl.tools</groupId>
                                <artifactId>checkstyle</artifactId>
                                <version>8.10</version>
                            </dependency>
                            <dependency>
                                <groupId>com.gitlab.ttubbs</groupId>
                                <artifactId>codeanalyzers</artifactId>
                                <version>${codeanalyzers.version}</version>
                            </dependency>
                        </dependencies>
                        <configuration>
                            <encoding>UTF-8</encoding>
                            <consoleOutput>false</consoleOutput>
                            <failsOnError>false</failsOnError>
                            <linkXRef>false</linkXRef>
                            <includeTestSourceDirectory>true</includeTestSourceDirectory>
                            <logViolationsToConsole>${analysis.checkstyle.logToConsole}</logViolationsToConsole>
                            <maxAllowedViolations>${analysis.checkstyle.maxAllowedViolations}</maxAllowedViolations>
                            <failOnViolation>${analysis.checkstyle.failOnViolation}</failOnViolation>
                            <configLocation>checkstyle/checkstyle.xml</configLocation>
                            <suppressionsLocation>checkstyle/checkstyle.suppressions.xml</suppressionsLocation>
                        </configuration>
                    </plugin>

                    <!-- PMD -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-pmd-plugin</artifactId>
                        <version>3.4</version>
                        <executions>
                            <execution>
                                <id>pmd-analysis</id>
                                <phase>${analysis.pmd.phase}</phase>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                                <configuration>
                                    <failOnViolation>${analysis.pmd.failOnViolation}</failOnViolation>
                                </configuration>
                            </execution>
                            <execution>
                                <id>cpd-analysis</id>
                                <phase>${analysis.pmd.phase}</phase>
                                <goals>
                                    <goal>cpd-check</goal>
                                </goals>
                                <configuration>
                                    <failOnViolation>${analysis.cpd.failOnViolation}</failOnViolation>
                                </configuration>
                            </execution>
                        </executions>
                        <configuration>
                            <linkXRef>false</linkXRef>
                            <failOnViolation>${analysis.pmd.failOnViolation}</failOnViolation>
                            <printFailingErrors>${analysis.pmd.logToConsole}</printFailingErrors>
                            <verbose>false</verbose>
                            <includeTests>${analysis.pmd.includeTests}</includeTests>
                            <rulesets>
                                <ruleset>pmd/pmd-ruleset.xml</ruleset>
                            </rulesets>
                        </configuration>
                        <dependencies>
                            <dependency>
                                <groupId>com.gitlab.ttubbs</groupId>
                                <artifactId>codeanalyzers</artifactId>
                                <version>${codeanalyzers.version}</version>
                            </dependency>
                        </dependencies>
                    </plugin>

                    <!-- FindBugs -->
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>findbugs-maven-plugin</artifactId>
                        <version>3.0.3</version>
                        <executions>
                            <execution>
                                <id>findbugs-analysis</id>
                                <phase>${analysis.findbugs.phase}</phase>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <effort>Max</effort>
                            <threshold>Low</threshold>
                            <xmlOutput>true</xmlOutput>
                            <findbugsXmlOutputDirectory>${project.build.directory}/findbugs</findbugsXmlOutputDirectory>
                            <failOnError>${analysis.findbugs.failOnViolation}</failOnError>
                            <excludeFilterFile>findbugs/findbugs.exclude.xml</excludeFilterFile>
                            <includeTests>${analysis.findbugs.includeTests}</includeTests>
                        </configuration>
                        <dependencies>
                            <dependency>
                                <groupId>com.gitlab.ttubbs</groupId>
                                <artifactId>codeanalyzers</artifactId>
                                <version>${codeanalyzers.version}</version>
                            </dependency>
                        </dependencies>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
```

For convienience, these configuration files may be used to configure IDE's and their plug-ins in order
to provide consitent analysis no matter develpers preferred tools.  The following profile will pull the configuration
files from Artifactory to the local file system and running to sync whenevery desired makes it easy to ensure the local
tools always have the latest changes.  See the guides for specific tools on how to configure then using these files.
```xml
    <profiles>

        <!-- Pull conf files from Artifactory to local file system for use in IDE's and/or Plug-ins -->
        <profile>
            <id>pull-analysis-conf-files</id>
            <activation>
                <property>
                    <!-- Activate by setting property. e.g. vm arg -Dsync-conf-files=true -->
                    <name>sync-conf-files</name>
                </property>
            </activation>
            <properties>
                <codeanalyzers.version>trunk-SNAPSHOT</codeanalyzers.version>
                <local.location>C:/dev/tools/analyzers/conf</local.location>
            </properties>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-dependency-plugin</artifactId>
                        <executions>

                            <execution>
                                <id>unpack-codeanalyzers</id>
                                <phase>initialize</phase>
                                <goals>
                                    <goal>unpack</goal>
                                </goals>
                                <configuration>
                                    <artifactItems>
                                        <artifactItem>
                                            <groupId>com.gitlab.ttubbs</groupId>
                                            <artifactId>codeanalyzers</artifactId>
                                            <type>jar</type>
                                            <version>${codeanalyzers.version}</version>
                                            <includes>**/checkstyle/*,**/findbugs/*,**/pmd/*</includes>
                                            <outputDirectory>${local.location}</outputDirectory>
                                        </artifactItem>
                                    </artifactItems>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    <profiles>
```

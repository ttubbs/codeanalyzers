# IntelliJ

## Configuration

* Import Custom Style
  `File -> Settings -> Editor -> Code Style` choose the `config button(gear/cog`) and `Import Scheme` to import this `code-style.xml` file

## Clean Code

Per code file:
* `Code -> Optimize Imports`
* `Code -> Reformat Code`
* Add Javadoc for public methods (required for methods more than 4 lines)


## Analyze Code

### Analyze using Maven

* See the [Maven](src/main/conf/maven/) instructions to configure the project pom
* In IntelliJ's Maven Projects window, check the `code-analysis` Profile then RIGHT+CLICK `compile` and choose `Run 'my project [compile]'`

![Maven run command](maven-analyze.png)

### Analyze using Plug-ins
- Install the CheckStyle plug-in: `File -> Settings...` then `Plugins`

    ![Maven run command](checkstyle-plugin-01.png)

- Configure plug-in to use the CheckStyle rules
    - Save the configuration files to your local file system.  Recommended location: `C:\dev\tools\analyzers\conf`  
      See the [Maven](src/main/conf/maven/) instructions to configure the `pull-anaylsis-conf-files` profile to automate this step
    - Configure the plugin as shown below.  
      Configure Globally: `File -> Other Settings -> Default Settings...` then `Other Settings -> CheckStyle`  
      Configure Per Project: `File -> Settings` then `Other Settings -> CheckStyle`  

    ![Maven run command](checkstyle-plugin-02.png)

    ![Maven run command](checkstyle-plugin-03.png)

- Run CheckStyle Scan on Project  
    
    ![Maven run command](checkstyle-plugin-04.png)

# Eclipse JEE Oxygen.3

These instructions were based on using a fresh download and install of Eclipse JEE Oxygen.3, but should apply to any Oxygen version of Eclipse.  Eclipse has not maintained
consistency in its styling tools across versions, so something other than Oxygen may require further adjustments.

## Configuration

* Import Custom Java Code Formatter
  `Window -> Preferences -> Java -> Code Style -> Formatter` choose the `Import...` button and import this `java-code-formatter.xml`

## Clean Code

Per code file:
* `Source -> Organize Imports`
* `Source -> Format`
* Add Javadoc for public methods (required for methods more than 4 lines)

## Analyze Code

### Analyze using Maven

* See the [Maven](src/main/conf/maven/) instructions to configure the project pom
* In Eclipse, RIGHT+CLICK on project then `Run As -> Maven build...` then configure as shown below.  
  A goal of `verify` will cause CheckStyle and PMD to execute, while a goal of `compile` will also execute FindBugs. Notice that both the Profile `code-analysis` and parameter `analyze` have been set to show the different ways to activate the profile, but only one or the other is necessary.

![Maven run command](maven-analyze.png)

### Analyze using Plug-ins

 - TODO: Add info on how to use the rules from this project in the CheckStyle, PMD and FindBugs plug-ins

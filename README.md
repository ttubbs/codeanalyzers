# Code Analyzers
**Code Analyzers** is a utility library that enables developers to
validate adherence to **_coding standards_**, or **_best practices_**,
locally rather than waiting for build pipeline results.

## Introduction

The goal of code analyzers is to promote consistency and foster clean code.
If something like [Sonar]( https://www.sonarqube.org/),
is being used in the build process in order to automate and
provide a dashboard to report on the quality and health of projects, then
the goal of this utility would be to mimic the same rules used in the pipeline
so that they can be executed locally, providing feedback quicker than checking
code into git and waiting for the results to appear on a dashboard. 

## Coding Style & Standards

Code analysis tools are opinionated and subjective, and it is not expected that every developer will agree with all of them.  We’ve adopted the rules used by Google, Sun and others, and tweaked them where necessary to accommodate our tools and methodology.  Our philosophy on the use and customization of the rules, more or less in this priority order, is:

- We use rules out-of-the-box as much as possible, preferring to customize only when necessary to integrate with other tools (IDEs, plugins) or prevent build failures that cannot be fixed otherwise.
- We prefer to do the simplest things to 'fix' violations - that may be customizing the IDE formatters to comply with the rules or tweaking the rulesets to match our environment.
- We prefer that a rule be on or off, not simply turning down the severity to warning.  In our experience, when turning down issues they never get fixed and the build output becomes full of noise.
- Turn off or down rules that are a bigger burden then they add value.  E.g. Requiring JavaDoc for every member and method has been turned down to only public scoped members and methods.
- Disputes between out-of-the-box and personal preference typically side with out-of-the-box 

## Development & Deployment Tools

Many of the tools we use for development and deployment have their own set of default standards they use to auto-apply or report violations found in our code.
Locally we make use of multiple IDEs that all have their own set of plug-ins, we use Maven which has its own plug-ins, and eventually push code to git where
Jenkins and other SCM tools have their own rules.  All of these tools need to be configured so they work well together.  For information on how to configure your preferred tools to comply with the rules,
follow these links:

-   [Maven](src/main/conf/maven/)
-	[Eclipse JEE Oxygen.3](src/main/conf/Eclipse-JavaEE-Oxygen3a.4.7.3a/)
-	[IntelliJ 2017.1.4](src/main/conf/IntelliJ-2017.1.4/)
